`TTD Parser <https://gitlab.inria.fr/aistrosight/ttd_parser>`_ Documentation
============================================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    readme
    modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
