#!/usr/bin/env python3
'''
Field value class.
'''

import pandas as pd


class FieldValue():
    '''
    Convenience class for accessing field values.

    Attributes:
        field:
            The name of the field.

        rows:
            The list of rows. Each row is a list of values.

    '''
    def __init__(self, field, abbreviations=None, n_values=None):
        '''
        Args:
            field:
                The field name.

            abbreviations:
                A optional list of abbreviations defined for this field. If
                n_values is None, it will be set from the length of this list if
                given.

            n_values:
                The number of values for each row of this field. If not given,
                it can be determined from the number of abbreviations.
        '''
        self._field = field
        self._abbreviations = abbreviations

        if n_values is None and abbreviations is not None:
            n_values = len(abbreviations)
        self.n_values = n_values

        self.rows = []

    @property
    def abbreviations(self):
        '''
        The abbreviations associated with this field.
        '''
        if self._abbreviations:
            return self._abbreviations[:]
        return None

    @abbreviations.setter
    def abbreviations(self, abbreviations):
        '''
        Setter for the abbreviations property.
        '''
        self._abbreviations = list(abbreviations)

    def append(self, row):
        '''
        Append a new row of values.

        Args:
            row:
                The list of values to append.
        '''
        if not isinstance(row, list):
            row = list(row)
        if self.n_values:
            row = row[:self.n_values]
        self.rows.append(row)

    def trim(self):
        '''
        Trim trailing empty columns from the internal table of values.
        '''
        n_values = 0
        n_max = 0
        for row in self.rows:
            for i, item in enumerate(row, start=1):
                if item:
                    n_values = max(n_values, i)
                n_max = max(n_max, i)

        # Trim empty columns.
        if n_values < n_max:
            self.rows = [row[:n_values] for row in self.rows]

        self.n_values = n_values

    def __len__(self):
        '''
        The number of rows provided for this field.
        '''
        return len(self.rows)

    def __iter__(self):
        '''
        Iterate over value for the field. If there is only one value per row
        then this iterates over single value items, otherwise it iterates over
        lists of values.
        '''
        n_values = self.n_values
        if n_values > 1:
            yield from (row[:n_values] for row in self.rows)
        else:
            yield from (row[0] for row in self.rows)

    def rows_as_dicts(self):
        '''
        Iterate over the rows as dicts. This is only possible if the
        abbreviations was set.
        '''
        abbreviations = self._abbreviations
        if not abbreviations:
            raise ValueError('No abbreviations set.')
        for row in self.rows:
            yield dict(zip(abbreviations, row))

    @property
    def dataframe(self):
        '''
        The values as a dataframe. If abbreviations are set then they will be
        used to set the column names.
        '''
        try:
            return pd.DataFrame(self.rows_as_dicts)
        except ValueError:
            return pd.DataFrame(self.rows).T

    @property
    def value(self):
        '''
        The first value of the first row. Use this to access fields that only
        appear once with one value.

        Returns:
            The value if set, otherwise None.
        '''
        try:
            return self.rows[0][0]
        except IndexError:
            return None

    @property
    def values(self):
        '''
        The values of the first row. Use this to access multi-value fields that
        only appear once.

        Returns:
            The list of values if set, otherwise an empty list.
        '''
        try:
            return self.rows[0][:]
        except IndexError:
            return []
