#!/usr/bin/env python3
'''
Functions for generating scripts when the package is installed.
'''

import argparse
import inspect
import logging


from ttd_parser.parser import Parser


LOGGER = logging.getLogger(__name__)


def configure_logging(level=logging.INFO):
    '''
    Configure logging.

    Args:
        level:
            The logging level.
    '''
    logging.basicConfig(
        style='{',
        format='[{asctime}] {levelname} {message}',
        datefmt='%Y-%M-%D %H:%M:%S',
        level=level
    )


def get_argparser(*args, **kwargs):
    '''
    Get an ArgumentParser object with the description set to the docstring of
    the calling function.

    Args:
        *args:
            Positional arguments passed through to ArgumentParser.

        **kwargs:
            Keyword arguments passed through to ArgumentParser. If the
            description is given, it will be overridden.
    '''
    caller = inspect.stack()[1][3]
    kwargs['description'] = inspect.getdoc(globals()[caller])
    return argparse.ArgumentParser(*args, **kwargs)


def parse_and_save(args=None):
    '''
    Parse a TTD file and save it to a new file. This serves as a check on the
    preservatin of internal data and ensures that the output file has the
    expected structure such as the correct number of fields in each row in the
    TSV data section.
    '''

    configure_logging()

    argparser = get_argparser()
    argparser.add_argument('inpath', help='The input path.')
    argparser.add_argument('outpath', help='The output path.')
    pargs = argparser.parse_args(args=args)

    Parser(path=pargs.inpath).save(pargs.outpath)
