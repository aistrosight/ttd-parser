#!/usr/bin/env python3
'''
Parse files that follow the Therapeutic Targets Database (TTD) file format:
https://db.idrblab.net/ttd/
'''

import logging
import pathlib
from collections import OrderedDict

from ttd_parser.field_value import FieldValue


LOGGER = logging.getLogger(__name__)


class Parser():
    '''
    Parse files that follow the Therapeutic Targets Database (TTD) file format.
    These files consist of 3 sections.

    The first contains metadata in text format. It is stored as a block of text.

    The second section is usually labelled "abbreviations" or "abbreviations
    index". It is delimited by lines of dashes and contains a single annotation
    line followed by a table of tab-separated values (TSV format). The first
    column contains field names used in the third section of the file (described
    below) and the remaining values indicate the items associated with the given
    field. This section is stored as an OrderedDict mapping field names to lists
    of values.

    The third and final section is a TSV table. The first column contains an
    identifier and the second a field name. The remaining columns contain values
    associated with the field name. Some of these values may be empty as not all
    fields take the same number of values but the TSV format requires all lines
    to have the same number of values. The field name can be looked up in the
    abbreviations section to determine how many values it should have. Field
    names can be repeated multiple times for the same identifier. This section
    is therefore stored as an OrderedDict mapping identifiers to OrderedDicts.
    The internal OrderedDict maps field names to instances of FieldValue, which
    provides different methods to facilitate access to the underlying data.

    OrderedDicts are used to preserve the order when recreating the file via
    save().
    '''
    ENCODING = 'utf-8'
    SECTION_DELIMITER_PREFIX = '-----'
    FIELD_DELIMITER = '\t'

    def __init__(self, path=None):
        '''
        Args:
            path:
                An optional path to load. The path can also be loaded later with
                load().
        '''
        self.metadata = ''
        self.abbreviations = OrderedDict()
        self.data = OrderedDict()
        self._section_delimiter_length = 80

        if path is not None:
            self.load(path)

    def _parse_metadata(self, lines):
        '''
        Parse the file metadata.

        Args:
            lines:
                The open file handle.
        '''
        metadata_lines = []
        for line in lines:
            if line.startswith(self.SECTION_DELIMITER_PREFIX):
                self._section_delimiter_length = len(line)
                break
            metadata_lines.append(line)
        self.metadata = '\n'.join(metadata_lines)

    def _parse_abbreviations(self, lines):
        '''
        Parse the abbreviations section.

        Args:
            handle:
                The open file handle.
        '''
        field_delim = self.FIELD_DELIMITER
        for line in lines:
            if line.startswith(self.SECTION_DELIMITER_PREFIX):
                break

            fields = line.rstrip(field_delim).split(field_delim)
            if len(fields) < 2:
                LOGGER.warning(
                    'Skipping line in abbreviations section: %s',
                    line
                )
                continue

            self.abbreviations[fields[0]] = fields[1:]

    def _parse_data(self, lines):
        '''
        Parse the data section.

        Args:
            handle:
                The open file handle.
        '''
        missing_abbreviations = set()
        for line in lines:
            if not line.strip():
                continue
            identifier, field, *row = line.split(self.FIELD_DELIMITER)
            try:
                abbreviation = self.abbreviations[field]
            except KeyError:
                abbreviation = None
                missing_abbreviations.add(field)
            else:
                n_expected_values = len(abbreviation)
                row = row[:n_expected_values]
            self.data.setdefault(
                identifier,
                OrderedDict()
            ).setdefault(
                field,
                FieldValue(
                    field,
                    abbreviation=abbreviation
                )
            ).append(row)
        for field in sorted(missing_abbreviations):
            LOGGER.warning('Field %s not found in abbreviations', field)

    def load(self, path):
        '''
        Load a file.

        Args:
            path:
                The path to the file.
        '''
        path = pathlib.Path(path).resolve()
        self.path = path
        self.metadata = ''
        self.abbreviations.clear()
        self.data.clear()

        LOGGER.info('Loading data from file %s', self.path)
        with self.path.open('r', encoding=self.ENCODING) as handle:
            lines = (line.rstrip('\n') for line in handle)
            self._parse_metadata(lines)
            self._parse_abbreviations(lines)
            self._parse_data(lines)

        # Trim empty columns from field values for the number of values is not
        # yet set.
        for fields in self.values():
            for field_value in fields.values():
                field_value.trim()

    @property
    def n_data_fields(self):
        '''
        Get the number of data fields for the TSV data section.
        '''
        n_fields = max(
            field_value.n_values
            for fields in self.data.values()
            for field_value in fields.values()
        )
        return n_fields + 2

    def __iter__(self):
        '''
        Iterate over all lines in the data section.
        '''
        yield from self.data

    def __getitem__(self, key):
        return self.data[key]

    def __contains__(self, key):
        return key in self.data

    def get(self, identifier, default=None):
        '''
        Get the value of an identifier. Return the default if it is not in the data.
        '''
        return self.data.get(identifier, default)

    def keys(self):
        '''
        The identifier keys of the data section.
        '''
        return self.data.keys()

    def values(self):
        '''
        The OrderdDict mapping field names to FieldValue objects for each
        identifier.
        '''
        return self.data.values()

    def items(self):
        '''
        Iterate over all identifier-OrderedDict pairs in the data section.
        '''
        return self.data.items()

    def _write_metadata(self, handle):
        '''
        Write the metadata section to a file handle.

        Args:
            handle:
                The open file handle.
        '''
        handle.write(self.metadata)
        handle.write('\n')

    def _write_abbreviations(self, handle):
        '''
        Write the metadata section to a file handle.

        Args:
            handle:
                The open file handle.
        '''
        section_delimiter = '-' * self._section_delimiter_length + '\n'
        field_delimiter = self.FIELD_DELIMITER

        handle.write(section_delimiter)
        handle.write('Abbreviations:\n')
        for name, value in self.abbreviations.items():
            handle.write(f'{name}{field_delimiter}{field_delimiter.join(value)}\n')
        handle.write(section_delimiter)
        handle.write('\n')

    def _write_data(self, handle):
        '''
        Write the metadata section to a file handle.

        Args:
            handle:
                The open file handle.
        '''
        field_delimiter = self.FIELD_DELIMITER
        expected_n_fields = self.n_data_fields

        # -1 because we need a separator between each field but not at the end
        empty_line = field_delimiter * (expected_n_fields - 1) + '\n'

        last_identifier = None
        for identifier, fields in self.items():
            # Add the empty lines before each new identifier.
            if identifier != last_identifier:
                if last_identifier is not None:
                    handle.write(empty_line)
                last_identifier = identifier
            for field, field_value in fields.items():
                for row in field_value.rows:
                    fields = [identifier, field, *row]
                    handle.write(field_delimiter.join(fields))
                    # No -1 here because each missing field should be
                    # preceded by a delimiter.
                    missing = expected_n_fields - len(fields)
                    if missing > 0:
                        handle.write(field_delimiter * missing)
                    handle.write('\n')

    def save(self, path):
        '''
        Save a file.

        Args:
            path:
                The path to the file.
        '''

        path = pathlib.Path(path).resolve()
        LOGGER.info('Saving data to file %s', path)
        path.parent.mkdir(parents=True, exist_ok=True)
        with path.open('w', encoding=self.ENCODING) as handle:
            self._write_metadata(handle)
            self._write_abbreviations(handle)
            self._write_data(handle)

    @property
    def identifiers(self):
        '''
        The set of identifiers.
        '''
        return set(self.data)

    @property
    def all_fields(self):
        '''
        The set of all fields. Not everyidentifier will have all fields.
        '''
        all_fields = set()
        for fields in self.data.values():
            all_fields.update(fields)
        return all_fields

    @property
    def common_fields(self):
        '''
        The set of common fields for all identifiers.
        '''
        common_fields = None
        for _identifier, fields in self.data.items():
            fields = set(fields)
            if common_fields is None:
                common_fields = fields
            else:
                common_fields &= fields
        return common_fields

    def filter_by_required_fields(self, required_fields):
        '''
        Generator over all items that contains the required fields.

        Args:
            required_fields:
                The iterable of required fields.

        Returns:
            A generator over the items that contain the required fields.
        '''
        required_fields = set(required_fields)
        for identifier, fields in self.data.items():
            if all(field in fields for field in required_fields):
                yield identifier, fields
