---
title: README
author: Jan-Michael Rye
---

# Synopsis

Parse files that follow the [Therapeutic Targets
Database](https://db.idrblab.net/ttd/) (TTD) file format.

# Links

* [Source code](https://gitlab.inria.fr/aistrosight/ttd-parser)
* [API documentation](https://aistrosight.gitlabpages.inria.fr/ttd-parser)

# Usage

## Python API

Some basic usage examples are provided below. Consult the [API documentation](https://aistrosight.gitlabpages.inria.fr/ttd-parser) for full usage.

~~~python
from ttd_parser import Parser

ttd = Parser('/path/to/TTD_file')

# Iterate over all of the data.
for identifier, fields in ttd.items():
    print(identifier)
    for field, field_value in fields.items():
        print(f'{  field}')
        for row in field_value:
            print(f'{    row}')

# The set of identifiers.
identifiers = ttd.identifiers

# The set of all fields seen. Not every identifier will have every field.
all_fields = ttd.all_fields

# The set of common fields shared by every identifier.
common_fields = ttd.common_fields

# The set of fields that are missing for some identifiers.
missing_fields = all_fields - common_fields

# Get the data associated with the identifier "T12345".
data = ttd['T12345']

# Check if the file contains the identifier "T12345".
if 'T12345' in ttd:
    print('Found T12345')

# The parser passes through several methods to the underlying dict.
identifiers = ttd.keys()
all_field_dicts = ttd.values()
fields = ttd.get('T12345', default=None)
for identifier in ttd:
  print(identifier)

# Get the values of the TARGETID field for the identifier "T12345". This should
# be a single value so we can use the FieldValue "value" attribute.
values = ttd['T12345']['TARGETID'].value

# Print a table of highest clinical status for each drug name per identifier.
# This is possible if DRUGINFO is listed in the abbreviations section.
for identifier, fields in ttd:
    druginfo = fields.get('DRUGINFO')
    if druginfo:
        for row in druginfo.rows_as_dicts():
            print(identifier, row['Drug Name'], row['Highest Clinical Status'])

# Get a Pandas DataFrame containing all of the INDICATI field data for the
# identifier T12345.
indicati = ttd['T12345']['INDICATI'].dataframe
~~~

## Scripts

The following scripts are also installed with the package.

### ttd-parse_and_save

~~~
$ ttd-parse_and_save -h
usage: scripts.py [-h] inpath outpath

Parse a TTD file and save it to a new file. This serves as a check on the
preservatin of internal data and ensures that the output file has the expected
structure such as the correct number of fields in each row in the TSV data
section.

positional arguments:
  inpath      The input path.
  outpath     The output path.

options:
  -h, --help  show this help message and exit
~~~
